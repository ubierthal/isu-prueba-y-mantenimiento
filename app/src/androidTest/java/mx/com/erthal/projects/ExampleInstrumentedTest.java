package mx.com.erthal.projects;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Properties;

import mx.com.erthal.projects.model.AppDatabase;
import mx.com.erthal.projects.model.Project;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("mx.com.erthal.projects", appContext.getPackageName());
    }


    @Test
    public void addProyect(){
        Context appContext = InstrumentationRegistry.getTargetContext();
        AppDatabase database = AppDatabase.getInMemoryDatabase(appContext);

        String name = "Proyecto de prueba";
        String startDate = "24/04/2018";
        String estimateDate = "24/04/2020";
        String description = "Description";
        String location = "Qro";

        Project project = new Project();
        project.name = name;
        project.startDate = startDate;
        project.estimateDate = estimateDate;
        project.description = description;
        project.location = location;

        int count = database.projectDao().getAll().size();
        database.projectDao().insertAll(project);
        int newCount = database.projectDao().getAll().size();
        assertEquals(count+1,newCount);

    }
}
