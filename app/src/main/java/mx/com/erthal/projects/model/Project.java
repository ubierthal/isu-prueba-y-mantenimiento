package mx.com.erthal.projects.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by Ubirajara on 21/04/2019.
 */

@Entity(tableName = "project")
public class Project {
    @PrimaryKey (autoGenerate = true)
    @NonNull
    public int uid;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "location")
    public String location;

    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "start_date")
    public String startDate;

    @ColumnInfo(name = "estimate_date")
    public String estimateDate;

    @ColumnInfo(name = "photos")
    public String photos;


}
