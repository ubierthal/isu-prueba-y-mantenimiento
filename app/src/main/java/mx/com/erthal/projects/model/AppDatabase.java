package mx.com.erthal.projects.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by Ubirajara on 21/04/2019.
 */

@Database(entities = {Project.class}, version = 2 , exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ProjectDao projectDao();


    private static AppDatabase INSTANCE;

    public static AppDatabase getInMemoryDatabase(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, "Sample.db")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
           // INSTANCE = Room.inMemoryDatabaseBuilder(context,AppDatabase.class).allowMainThreadQueries().fallbackToDestructiveMigration().build();
        }
        return  INSTANCE;
    }

    public static void  destroyInstance(){
        INSTANCE = null;
    }
}