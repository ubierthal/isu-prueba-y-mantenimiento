package mx.com.erthal.projects.view;

import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mx.com.erthal.projects.R;
import mx.com.erthal.projects.model.AppDatabase;
import mx.com.erthal.projects.model.Project;

public class AddProjectActivity extends AppCompatActivity {

    private AppDatabase db;
    private EditText name, location, description, startDate, estimateDate;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_proyect);

        db = AppDatabase.getInMemoryDatabase(this);

        initComponents();
        setEvents();
        loadView();


    }

    private void initComponents() {
        name = findViewById(R.id.name);
        location = findViewById(R.id.location);
        description = findViewById(R.id.description);
        startDate = findViewById(R.id.start_date);
        estimateDate = findViewById(R.id.estimate_date);
    }

    private void setEvents() {

    }

    private void loadView() {

        startDate.setText(dateFormat.format(new Date()));
        estimateDate.setText(dateFormat.format(new Date()));
    }

    public void save(View view) {

        if (valid()) {
            try {
                Project project = new Project();
                project.name = name.getText().toString();
                project.description = description.getText().toString();
                project.estimateDate = estimateDate.getText().toString();
                project.location = location.getText().toString();
                project.startDate = startDate.getText().toString();

                //For all the RxJava or RxAndroid or RxKotlin lovers out there
                new AddTask(project).execute();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, R.string.add_project_fail, Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean valid() {
        boolean valid = true;
        name.setError(null);
        description.setError(null);
        startDate.setError(null);
        estimateDate.setError(null);
        location.setError(null);

        if (name.getText().toString().isEmpty()) {
            name.setError(getString(R.string.name_required));
            valid = false;
        }
        if (description.getText().toString().isEmpty()) {
            description.setError(getString(R.string.description_required));
            valid = false;
        }
        if (startDate.getText().toString().isEmpty()) {
            startDate.setError(getString(R.string.startDate_required));
            valid = false;
        }
        if (estimateDate.getText().toString().isEmpty()) {
            estimateDate.setError(getString(R.string.estimateDate_required));
            valid = false;
        }
        if (location.getText().toString().isEmpty()) {
            location.setError(getString(R.string.location_required));
            valid = false;
        }

        try {
            dateFormat.parse(estimateDate.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
            estimateDate.setError(getString(R.string.date_format_invalid));
            valid = false;
        }

        try {
            dateFormat.parse(startDate.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
            startDate.setError(getString(R.string.date_format_invalid));
            valid = false;
        }
        return valid;
    }

    private class AddTask extends AsyncTask<Void, Void, String> {

        private Project project;

        public AddTask(Project project) {
            this.project = project;
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                db.projectDao().insertAll(project);
                return "";
            }
            catch (Exception e){
                e.printStackTrace();
                return e.getMessage();

            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.isEmpty()) {
                Toast.makeText(AddProjectActivity.this, R.string.add_project_success, Toast.LENGTH_LONG).show();
                setResult(RESULT_OK);
                finish();
            }
            else {
                Toast.makeText(AddProjectActivity.this, R.string.add_project_error, Toast.LENGTH_LONG).show();
            }

        }
    }
}
