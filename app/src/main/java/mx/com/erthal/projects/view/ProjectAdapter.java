package mx.com.erthal.projects.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import mx.com.erthal.projects.R;
import mx.com.erthal.projects.model.Project;

/**
 * Created by Ubirajara on 30/11/2017.
 */

public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.MyViewHolder> {
    private List<Project> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        TextView name;
        TextView location;
        TextView startDate;
        TextView estimateDate;

       // ProjectItemBinding projectItemBinding;

        public MyViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
            startDate = v.findViewById(R.id.start_date);
            location = v.findViewById(R.id.location);
            estimateDate = v.findViewById(R.id.estimate_date);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProjectAdapter(List<Project> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ProjectAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_item, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.name.setText(mDataset.get(position).name);
        holder.startDate.setText(mDataset.get(position).startDate);
        holder.estimateDate.setText(mDataset.get(position).estimateDate);
        holder.location.setText(mDataset.get(position).location);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
